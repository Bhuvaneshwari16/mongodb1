const mongoose = require('mongoose');
const config = require('config');
const bodyParser=require('body-parser');
const db='mongodb+srv://bhuvaneshwari:bhuvaneshwari@cluster0.u6otz.mongodb.net/myFirstDatabase?retryWrites=true&w=majority'

const connectDB=async () =>{
    try{
       await mongoose.connect(db,{
         useNewUrlParser:true,
         useUnifiedTopology:true,
         useCreateIndex:true
       });

       console.log('MongoDB connected...');
    }catch(err){
      console.error(err.message);

      //Exit process with  failure
      process.exit(1);    
    }
};
module.exports=connectDB;