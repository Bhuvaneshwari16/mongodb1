const express = require('express');
const connectDB=require('./config/database')
const app =express();

const cors = require("cors");
app.use(
  cors({
    origin: "*",                    // allow to server to accept request from different origin
    methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
    credentials: true,                    // allow session cookie from browser to pass through
  })
);

app.use(express.json());

app.use(express.urlencoded({ extended: true }));

// connect Database
connectDB();

// init middleware


app.get('/',(req,res)=>res.send('API Running'));
// define Routes
app.use('/api/users',require('./routes/api/users'))
app.use('/api/profile',require('./routes/api/profile'))
app.use('/api/auth',require('./routes/api/auth'))
app.use('/api/posts',require('./routes/api/posts'))

const PORT = process.env.PORT||5000;

app.listen(PORT,() => console.log(`Server started on port ${PORT}`));
