
import './App.css';
import React,{Fragment,useEffect} from "react";
// import {Routes , Route,Router} from 'react-router-dom'
// import { BrowserRouter as Router, Routes, Route } from 'react-router-dom';
import {BrowserRouter as Router,Route,Switch} from 'react-router-dom';
import Navbar from "./components/layout/Navabar";
import Landing from "./components/layout/Landing";
import Alert from "./components/layout/Alert";
import Register from "./components/auth/Register";
import Login from "./components/auth/Login";

//REDUX
import { Provider } from 'react-redux';
import store from "./store";
import { loadUser } from './actions/auth';
import setAuthToken from './utils/setAuthToken';
import Dashboard from "./components/dashboard/Dashboard"
import PrivateRoute from "./components/routing/PrivateRoute"
import CreateProfile from "./components/profile-forms/CreateProfile"
import EditProfile from "./components/profile-forms/EditProfile";
import AddExperience from "./components/profile-forms/AddExperience";
import AddEducation from "./components/profile-forms/AddEducation";
import Profiles from "./components/profiles/Profiles";

// import auth from"./componets/auth"

if(localStorage.token){
  setAuthToken(localStorage.token);
}



function App() {
  useEffect(()=>{
    store.dispatch(loadUser())
  },[])

  // [] means =>If you want to run an effect and clean it up only once (on mount and unmount), you can pass an empty array ([]) 
  //as a second argument. This tells React that your effect doesn’t depend
  // on any values from props or state, so it never needs to re-run.

  return (

    <Provider store ={store}>
    <Router>
    <Fragment >
          <Navbar/>
          <Route exact path = '/' component={Landing}/>
          <section className='container'>
            <Alert/>
            <Switch>
              <Route exact path="/register" component={Register}/>
              <Route  exact path="/login" component={Login}/> 
              <Route  exact path="/profiles" component={Profiles}/> 
            
             <PrivateRoute  exact path='/dashboard' component={Dashboard} /> 
             <PrivateRoute exact path ='/create-profile' component={CreateProfile}/>
              <PrivateRoute exact path ='/edit-profile' component={EditProfile}/>
               <PrivateRoute exact path ='/add-experience' component={AddExperience}/>
               <PrivateRoute exact path ='/add-education' component={AddEducation}/> 

            </Switch>
          </section>
        </Fragment>
      </Router>
      </Provider>



  )
}

export default App;